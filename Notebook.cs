﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NotebookApp
{
    public static class Notebook
    {

        private static readonly Predicate<String> isLetterOnlyString = value =>
            value.Count(Char.IsLetter) == value.Length;

        private static readonly Predicate<String> inDigitOnlyString = value =>
            value.Count(Char.IsDigit) == value.Length;

        private static readonly Predicate<String> isMainAction = value =>
            value.CompareTo("0") == 1 && value.CompareTo("7") == -1;

        private static readonly Predicate<String> isEditAction = value =>
            value.Length == 1 && Char.IsDigit(value[0]) && value != "0";

        private static readonly Predicate<String> isAnything = value => true;

        private static readonly String menuSeparator = " | ";

        private static readonly String defaultIncorrectMessagePrefix = "Введено неверное значение!" + Environment.NewLine;

        private static readonly String lettersIncorrectMessage = "Доступны только буквы!";

        private static readonly String digitsIncorrectMessage = "Доступны только цифры!";

        private static readonly String nonexistentIdMessage = "Записи с данным ID не существует." + Environment.NewLine;

        private static readonly String editMessage = "Изменить:" + Environment.NewLine +
                                                     "1 - Имя" + menuSeparator +
                                                     "2 - Фамилию" + menuSeparator +
                                                     "3 - Отчество" + menuSeparator +
                                                     "4 - Номер телефона" + menuSeparator +
                                                     "5 - Страну" + Environment.NewLine +
                                                     "6 - День рождения" + menuSeparator +
                                                     "7 - Организацию" + menuSeparator +
                                                     "8 - Должность" + menuSeparator +
                                                     "9 - Заметки";

        private static readonly String mainMessage = "Доступные команды:" + Environment.NewLine +
                                                     "1 - Добавить запись" + menuSeparator +
                                                     "2 - Изменить запись" + menuSeparator +
                                                     "3 - Удалить запись" + Environment.NewLine +
                                                     "4 - Показать запись по Id" + menuSeparator +
                                                     "5 - Показать все записи" + menuSeparator +
                                                     "6 - Выход";

        public static readonly Dictionary<Int32, Entry> Entries = new Dictionary<Int32, Entry>();

        public static Int32 NextId = 1;

        private static void Main(string[] args)
        {
            while (true)
            {
                switch (ReadValidString(isMainAction, mainMessage, defaultIncorrectMessagePrefix))
                {
                    case "1":
                        Add();
                        break;
                    case "2":
                        Edit();
                        break;
                    case "3":
                        Remove();
                        break;
                    case "4":
                        Show();
                        break;
                    case "5":
                        ShowAll();
                        break;
                    case "6":
                        Exit();
                        break;
                }
            }
        }

        private static void Add()
        {
            Entries.Add(NextId++, new Entry
            {
                FirstName = ReadValidString(isLetterOnlyString,
                    "Имя: ", defaultIncorrectMessagePrefix + lettersIncorrectMessage),
                SecondName = ReadValidString(isLetterOnlyString,
                    "Фамилия: ", defaultIncorrectMessagePrefix + lettersIncorrectMessage),
                PhoneNumber = ReadValidString(inDigitOnlyString,
                    "Номер телефона: ", defaultIncorrectMessagePrefix + digitsIncorrectMessage),
                Country = ReadValidString(isLetterOnlyString,
                    "Страна: ", defaultIncorrectMessagePrefix + lettersIncorrectMessage)
            });
        }

        private static void Edit()
        {
            Int32 id = ReadValidId();
            if(!Entries.ContainsKey(id))
                Console.WriteLine("Записи с данным ID не существует.");
            Entry entry = Entries[id];
            switch (ReadValidString(isEditAction, editMessage, defaultIncorrectMessagePrefix))
            {
                case "1":
                    entry.FirstName = ReadValidString(isLetterOnlyString,
                        "Имя: ", defaultIncorrectMessagePrefix + lettersIncorrectMessage);
                    break;
                case "2":
                    entry.SecondName = ReadValidString(isLetterOnlyString,
                        "Фамилия: ", defaultIncorrectMessagePrefix + lettersIncorrectMessage);
                    break;
                case "3":
                    entry.MiddleName = ReadValidString(isLetterOnlyString,
                        "Отчество: ", defaultIncorrectMessagePrefix + lettersIncorrectMessage);
                    break;
                case "4":
                    entry.PhoneNumber = ReadValidString(inDigitOnlyString,
                        "Номер телефона: ", defaultIncorrectMessagePrefix + digitsIncorrectMessage);
                    break;
                case "5":
                    entry.Country = ReadValidString(isLetterOnlyString,
                        "Страна: ", defaultIncorrectMessagePrefix + lettersIncorrectMessage);
                    break;
                case "6":
                    entry.Birthday = ReadValidString(isAnything, "День рождения: ", String.Empty);
                    break;
                case "7":
                    entry.Organization = ReadValidString(isAnything, "Организация: ", String.Empty);
                    break;
                case "8":
                    entry.Post = ReadValidString(isAnything, "Должность: ", String.Empty);
                    break;
                case "9":
                    entry.Notes = ReadValidString(isAnything, "Заметки: ", String.Empty);
                    break;
            }
        }

        private static void Remove()
        {
            Int32 id = ReadValidId();
            if (!Entries.ContainsKey(id))
            {
                Console.WriteLine(nonexistentIdMessage);
                return;
            }
            Entries.Remove(id);
        }

        private static void Show()
        {
            Int32 id = ReadValidId();
            if (!Entries.ContainsKey(id))
            {
                Console.WriteLine(nonexistentIdMessage);
                return;
            }
            Entry entry = Entries[id];
            Console.WriteLine("ID: " + id + Environment.NewLine +
                              "Имя: " + entry.FirstName + Environment.NewLine +
                              "Фамилию: " + entry.SecondName + Environment.NewLine +
                              "Отчество: " + entry.MiddleName + Environment.NewLine +
                              "Номер телефона: " + entry.PhoneNumber + Environment.NewLine +
                              "Страну: " + entry.Country + Environment.NewLine +
                              "День рождения: " + entry.Birthday + Environment.NewLine +
                              "Организацию: " + entry.Organization + Environment.NewLine +
                              "Должность: " + entry.Post + Environment.NewLine +
                              "Заметки: " + entry.Notes + Environment.NewLine);
        }

        private static void ShowAll()
        {
            foreach (var pair in Entries)
            {
                Console.WriteLine("ID: " + pair.Key + Environment.NewLine +
                                  "Имя: " + pair.Value.FirstName + Environment.NewLine +
                                  "Фамилию: " + pair.Value.SecondName + Environment.NewLine +
                                  "Номер телефона: " + pair.Value.PhoneNumber + Environment.NewLine);
            }
        }

        private static void Exit()
        {
            Environment.Exit(0);
        }

        private static Int32 ReadValidId()
        {
            return Int32.Parse(ReadValidString(inDigitOnlyString,
                "Введите ID: ", defaultIncorrectMessagePrefix + digitsIncorrectMessage));
        }

        private static String ReadValidString(Predicate<String> predicate, String initialMessage, String incorrectMessage)
        {
            Console.WriteLine(initialMessage);
            do
            {
                String input = Console.ReadLine();
                Console.WriteLine();
                if (predicate.Invoke(input))
                    return input;
                Console.WriteLine(incorrectMessage);
            } while (true);
        }

    }
}
