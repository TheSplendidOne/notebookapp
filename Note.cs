﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotebookApp
{
    public class Entry
    {
        public String FirstName { get; set; }

        public String SecondName { get; set; }

        public String MiddleName { get; set; }

        public String PhoneNumber { get; set; }

        public String Country { get; set; }

        public String Birthday { get; set; }

        public String Organization { get; set; }

        public String Post { get; set; }

        public String Notes { get; set; }
    }
}
